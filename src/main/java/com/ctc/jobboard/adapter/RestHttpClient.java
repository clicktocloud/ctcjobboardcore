package com.ctc.jobboard.adapter;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ctc.jobboard.exception.BadRequestException;

/**
 * This is delegator of standard HttpClient object and responses to build and forward a restful request to the standard methods of HttpClient.
 * The important thing is this delegator defines the success codes set for the business level, 
 * example for some job board service will return back a status code meaning success, but the status code is not 200, so we must re-define new status codes set including these non 200 code.
 * 
 * @author andy
 *
 */
public class RestHttpClient {
	
	
	private static int defaultRetryTimes = 4;
	
    private static Logger logger = LoggerFactory.getLogger("com.ctc.jobboard");
    
    private CloseableHttpClient httpclient;
    
    private static PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
    
    private Set<Integer> successCodes = new HashSet<Integer>(Arrays.asList(new Integer[]{200}));
    
   
    /**
     * Default contructor
     */
    public RestHttpClient(){
    	
    	httpclient  = HttpClients.createDefault();
    }
    
    /**
     * Constructor with parameters
     * @param minPoolSize
     * @param maxPoolSize
     * @param connectionTimeout
     * @param socketTimeout
     */
    public RestHttpClient(int minPoolSize, int maxPoolSize, int connectionTimeout, int socketTimeout){
    	super();
    	RequestConfig requestConfig = RequestConfig.custom()
    			.setConnectTimeout(connectionTimeout * 1000)
    			.setConnectionRequestTimeout(connectionTimeout * 1000)
    			.setSocketTimeout(socketTimeout * 1000).build();
    	
    	
    	// Total max connections
    	cm.setMaxTotal( maxPoolSize );
    	// Default max connection per route
    	cm.setDefaultMaxPerRoute( minPoolSize );
    	
    	// Increase max connections for localhost:80 to 50
    	//HttpHost localhost = new HttpHost("locahost", 80);
    	//cm.setMaxPerRoute(new HttpRoute(localhost), 50);
    	
    	httpclient  = HttpClientBuilder.create()
    			.setRetryHandler(new DefaultHttpRequestRetryHandler( defaultRetryTimes , false))
    			.setDefaultRequestConfig(requestConfig)
    			.setConnectionManager(cm)
    			.build();
    	
    	
    }
    
    /**
     * add a success code to success codes set
     * @param code
     */
    public void addSuccessCode(Integer code){
    	successCodes.add(code);
    }
    
    /**
     * add a success code set
     * @param codes
     */
    public void addSuccessCodes(Set<Integer> codes){
    	successCodes.addAll(codes);
    }
    

    public String post(String serviceEndpoint, String xml) throws Exception {
    	
    	Map<String, String> headers  = new HashMap<String,String>();
    	headers.put("Accept", "application/xml");
    	return post( serviceEndpoint,  xml, "application/xml",headers);
    	
    	
    }
    
    
    /**
     * execute a POST method
     * @param serviceEndpoint
     * @param bodyString
     * @param contentType
     * @param headers
     * @return
     * @throws Exception
     */
    public String post(String serviceEndpoint, String bodyString, String contentType, Map<String, String> headers) throws Exception {
    	
    	HttpPost httppost = new HttpPost(serviceEndpoint);
    	
    	return execute( httppost,  serviceEndpoint,  bodyString,  contentType,  headers);
    }
    
    /**
     * execute a PATCH method
     * @param serviceEndpoint
     * @param bodyString
     * @param contentType
     * @param headers
     * @return
     * @throws Exception
     */
   public String patch(String serviceEndpoint, String bodyString, String contentType, Map<String, String> headers) throws Exception {
    	
    	HttpPatch request = new HttpPatch(serviceEndpoint);
    	
    	return execute( request,  serviceEndpoint,  bodyString,  contentType,  headers);
    }
    
   /**
    * execute a PUT method
    * @param serviceEndpoint
    * @param bodyString
    * @param contentType
    * @param headers
    * @return
    * @throws Exception
    */
   public String put(String serviceEndpoint, String bodyString, String contentType, Map<String, String> headers) throws Exception {
    	
    	HttpPut httput = new HttpPut(serviceEndpoint);
    	
    	return execute( httput,  serviceEndpoint,  bodyString,  contentType,  headers);
    }
   
   /**
    * execute HttpEntityEnclosingRequestBase request
    * @param request
    * @param serviceEndpoint
    * @param bodyString
    * @param contentType
    * @param headers
    * @return
    * @throws Exception
    */
   public String execute(HttpEntityEnclosingRequestBase request, String serviceEndpoint, String bodyString, String contentType, Map<String, String> headers) throws Exception {
   	
	   	CloseableHttpResponse response = null;
	   
	   	setHttpRequestHeaders(request, headers);
	   	
	   	try {
	   		if(bodyString != null && ! bodyString.equals("")){
	   			StringEntity entity = new StringEntity(bodyString);
	       		if(contentType != null && !contentType.equals("")){
	       			entity.setContentType(contentType);
	       		}
	       		
	       		request.setEntity(entity);
	   		}
	   		
		    	//Execute and get the response.
		    	response = httpclient.execute(request);
		    	
		    	if(!successCodes.contains(response.getStatusLine().getStatusCode())){
		    		throw new BadRequestException(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
		    	}
		    	
		    	HttpEntity rEntity = response.getEntity();
	
		    	return EntityUtils.toString(rEntity,"UTF-8");
	   	} catch (Exception e) {
	   		logger.error("Failed to send out request", e);
	   		throw e;
	   	} finally {
	   		if (response != null) {
	   			try {
						response.close();
					} catch (IOException e) {
						logger.error("Unable to close the response.", e);
					}
	   		}
	   	}
   }
    
   /**
    * execute GET method
    * @param serviceEndpoint
    * @param headers
    * @return
    * @throws Exception
    */
    public String get(String serviceEndpoint,  Map<String, String> headers) throws Exception {
    	
    	CloseableHttpResponse response = null;
    	
    	HttpGet httpget = new HttpGet(serviceEndpoint);
    	
    	setHttpRequestHeaders(httpget, headers);
    	
    	try {
    		
	    	//Execute and get the response.
	    	
	    	response = httpclient.execute(httpget);
	    	HttpEntity rEntity = response.getEntity();
	    	
	    	if(!successCodes.contains(response.getStatusLine().getStatusCode() )){
	    		throw new BadRequestException(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
	    	}
	    	
	    	return EntityUtils.toString(rEntity,"UTF-8");
    	} catch (Exception e) {
    		logger.error("Failed to send out request", e);
    		throw e;
    	} finally {
    		if (response != null) {
    			try {
					response.close();
				} catch (IOException e) {
					logger.error("Unable to close the response.", e);
				}
    		}
    	}
    }
    
    /**
     * execute a HEAD method
     * @param serviceEndpoint
     * @param headers
     * @return
     * @throws Exception
     */
    public Map<String, String> head(String serviceEndpoint,  Map<String, String> headers) throws Exception {
    	
    	Map<String, String> httpHeaders = new HashMap<String, String>();
    	
    	CloseableHttpResponse response = null;
    	
    	HttpHead httphead = new HttpHead(serviceEndpoint);
    	
    	setHttpRequestHeaders(httphead, headers);
    	
    	try {
    		
	    	//Execute and get the response.
	    	
	    	response = httpclient.execute(httphead);
	    	
	    	if(!successCodes.contains(response.getStatusLine().getStatusCode() )){
	    		throw new BadRequestException(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
	    	}
	    	
	    	
	    	Header[] resultHeaders = response.getAllHeaders();
	    	if(resultHeaders != null ){
	    		
	    		for(Header header : resultHeaders){
	    			httpHeaders.put(header.getName(), header.getValue());
	    		}
	    			
	    	}
	    	
	    	return httpHeaders;
    	} catch (Exception e) {
    		logger.error("Failed to send out request", e);
    		throw e;
    	} finally {
    		if (response != null) {
    			try {
					response.close();
				} catch (IOException e) {
					logger.error("Unable to close the response.", e);
				}
    		}
    	}
    }
    
    
    private void setHttpRequestHeaders(HttpRequest request, Map<String, String> headers){
    	if(headers != null && headers.size() > 0 ){
    		for(Entry<String, String> header : headers.entrySet()){
    			request.setHeader(header.getKey(), header.getValue());
    		}
    		
    	}
    }
    
    public void finalize() {
    	if (httpclient != null) {
    		try {
    			
				httpclient.close();
			} catch (IOException e) {
				logger.error("Unable to close the response.", e);
			}
    	}
    	
    	if (cm != null) {
    		
			cm.close();
			
    	}
    }
    
    
  
    
}
