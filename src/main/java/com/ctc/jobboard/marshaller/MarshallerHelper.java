package com.ctc.jobboard.marshaller;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MarshallerHelper {
	 public static final String MEDIA_TYPE = "eclipselink.media-type";

	/**
     * Using Jaxb library to parse xml to POJO.
     *
     * @param xml   - xml file
     * @param clazz - The class of the target object
     * @param <T>   - The type of the object
     * @return
     * @throws JAXBException 
     */
    @SuppressWarnings("unchecked")
	public static <T> T convertXMLtoObject(String xmlString, Class<T> clazz) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		StringReader reader = new StringReader(xmlString);
		return (T) jaxbUnmarshaller.unmarshal(reader);
    }
    
    /**
     * Using Jaxb library to convert POJO to xml file.
     * 
     * @param object
     * @return
     * @throws JAXBException
     */
    public static <T> String convertObjecttoXML(T object) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
		StringWriter stringWriter = new StringWriter();
	    Marshaller marshaller = jaxbContext.createMarshaller();
	    marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	    marshaller.marshal(object, stringWriter);
	    return stringWriter.toString();
    }
    
   
    public static <T> T convertJsontoObject(String jsonString, Class<T> clazz) throws JAXBException {
    	
    	ObjectMapper mapper = new ObjectMapper();
	
		try {
			return (T) mapper.readValue(jsonString, clazz);
		} catch (JsonParseException e) {
			throw new JAXBException("JsonProcessingException - Invalid json content", e);
		} catch (JsonMappingException e) {
			throw new JAXBException("JsonMappingException - Invalid json content",e);
		} catch (IOException e) {
			throw new JAXBException("IOException - Failed to read json content",e);
		}
	
		
    }
    
    public static  String convertObjecttoJson(Object obj) throws JAXBException{
		ObjectMapper mapper = new ObjectMapper();
		
		//Object to JSON in String
		String jsonString = "";
		
		try {
			jsonString = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			throw new JAXBException("JsonProcessingException - Failed to convert object to json", e);
		}
		
		return jsonString;
	}
    
}
