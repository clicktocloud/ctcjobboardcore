package com.ctc.jobboard.exception;

public class JobBoardException extends RuntimeException {
	private static final long serialVersionUID = -6067294234822229081L;

	public JobBoardException() {
		super();
	}

	public JobBoardException(String message, Throwable cause) {
		super(message, cause);
	}

	public JobBoardException(String message) {
		super(message);
	}

	public JobBoardException(Throwable cause) {
		super(cause);
	}

}
