package com.ctc.jobboard.persistence;

import java.util.ArrayList;
import java.util.List;

public class JBConnectionException extends Exception {
	
	private List<String> details = new ArrayList<String>();
	
	private List<Error> errors = new  ArrayList<Error>();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JBConnectionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public JBConnectionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public JBConnectionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public List<String> getDetails() {
		return details;
	}

	public void setDetails(List<String> details) {
		this.details = details;
	}
	
	public void addDetails(String detail) {
		this.details.add(detail);
	}
	
	public List<Error> getErrors() {
		return errors;
	}

	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}
	
	public void addErrors(Error error) {
		this.errors.add(error);
	}

	public static class Error{
		private String message;
		private String[] fields;
		
		
		public Error(String message, String[] fields) {
			super();
			this.message = message;
			this.fields = fields;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public String[] getFields() {
			return fields;
		}
		public void setFields(String[] fields) {
			this.fields = fields;
		}
		
	}

}
