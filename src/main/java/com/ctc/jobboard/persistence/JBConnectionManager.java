package com.ctc.jobboard.persistence;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang.StringUtils;

import com.ctc.jobboard.util.BasicConfig;

@SuppressWarnings("unchecked")
public class JBConnectionManager {
	
	private static Class<? extends JBConnection> driverClass;
	
	static {
		
		try {
			String driverName = BasicConfig.get("jb_connection_driver");
			driverName = StringUtils.isEmpty(driverName) ? "com.ctc.jobboard.persistence.drivers.SfJBConnection" : driverName;
			driverClass = (Class<? extends JBConnection>) Class.forName(driverName);
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	public static synchronized JBConnection getConnection() throws JBConnectionException{
		if(driverClass != null){
			try {
				return driverClass.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				throw new JBConnectionException(e);
			}
			
		}
		
		return null;
	}
	
	public static synchronized JBConnection getConnection(String orgId) throws JBConnectionException{
		if(driverClass != null){
			try {
				Constructor<? extends JBConnection> cons = driverClass.getConstructor(String.class);
				
				
				return cons.newInstance(orgId);
			} catch (NoSuchMethodException | SecurityException
					| InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException e) {
				throw new JBConnectionException(e);
			}
			
		}
		
		return null;
	}
	
	public static JBConnection connect(String orgId) throws JBConnectionException{
		JBConnection conn = getConnection(orgId);
		if(conn != null){
			return conn.connect();
		}
		throw new JBConnectionException("ERROR : Failed to get connection to "+orgId);
	}
	
	public static synchronized void register(Class<? extends JBConnection> driverClass){
		JBConnectionManager.driverClass  = driverClass;
		
	}

}
