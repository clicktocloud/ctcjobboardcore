package com.ctc.jobboard.persistence;

import java.util.List;

import com.ctc.jobboard.component.IAccount;
import com.ctc.jobboard.util.SfOrg;

/**
 * This is abstraction of persistence instance, such as MySql, Salesforce 
 * and declares some standard data objects and a range of methods to interact with persistency layer.
 * It must be implemented when you implement job board application which needs to save data into specified persistence layer, 
 * such as mysql, salesforce.
 * 
 * @author andy
 *
 */
public interface JBConnection{
	
	public  Object getConn( );
	
	/**
	 * connect to an instance of persistence layer
	 * @return JBConnection
	 * @throws JBConnectionException
	 */
	public JBConnection connect() throws JBConnectionException;
	
	/**
	 * bulk update a list of records typed as JBObject to persistence layer
	 * @param jbObjects
	 * @throws JBConnectionException
	 */
	public void update(List<JBObject> jbObjects) throws JBConnectionException ;
	
	/**
	 * query persistence layer with query string and convert result to POJO object
	 * @param queryString
	 * @param resultClass
	 * @return
	 * @throws JBConnectionException
	 */
	public <T> T query(String queryString , Class<T> resultClass) throws JBConnectionException;
	
	/**
	 * retrieve more records from query result cursor
	 * @param queryString
	 * @param resultClass
	 * @return
	 * @throws JBConnectionException
	 */
	public <T> T queryMore(String queryString , Class<T> resultClass) throws JBConnectionException;
	
	/**
	 * get a job board account from persistence layer
	 * @param jobBoardName
	 * @param namespace
	 * @return
	 * @throws JBConnectionException
	 */
	public IAccount getAccount( String jobBoardName, String namespace) throws JBConnectionException;
	
	/**
	 * get a list of org information from persistence layer
	 * @param jobBoardSfApiName
	 * @return
	 * @throws JBConnectionException
	 */
	public  List<SfOrg>  getOrgList( String jobBoardSfApiName ) throws JBConnectionException;
	

}
