package com.ctc.jobboard.plugin;

import com.ctc.jobboard.core.JobBoardCredential;
import com.ctc.jobboard.core.JobBoardResponsePackage;
import com.ctc.jobboard.defaultlist.DefaultList;

public interface JobBoardPluginInterface {
	
	
	public void setJobBoardName(String boardName);
	
	public JobBoardResponsePackage createJob(String referenceNo, String postingStatus, String jobJsonContent,JobBoardCredential credential);
	public JobBoardResponsePackage updateJob(String referenceNo,String jobBoardAdId, String postingStatus, String jobJsonContent,JobBoardCredential credential);
	public JobBoardResponsePackage archiveJob(String referenceNo,String jobBoardAdId, String postingStatus,JobBoardCredential credential);
	public JobBoardResponsePackage getAllJobs(JobBoardCredential credential);
	public JobBoardResponsePackage getJob( String jobBoardAdId, JobBoardCredential credential);
	
	public DefaultList getDefaultList(JobBoardCredential credential);

}
