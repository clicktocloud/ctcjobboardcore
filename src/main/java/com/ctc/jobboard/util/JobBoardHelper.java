package com.ctc.jobboard.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

public class JobBoardHelper {
	
public static String readFileToString(String filename) throws IOException{
		
		
		InputStream stream = JobBoardHelper.class.getResourceAsStream(filename);
		
		String ret = IOUtils.toString(stream,"utf-8");
		
		stream.close();
		return ret;
	}

}
