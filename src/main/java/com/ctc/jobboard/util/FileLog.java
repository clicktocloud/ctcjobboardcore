package com.ctc.jobboard.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class responses to log the activities of pushing candidate to daxtra server and provides
 * two types of log: success and error log
 * 
 * @author andy
 *
 */
public class FileLog {
	
	private static final Logger LOGGER = LoggerFactory.getLogger("com.ctc.jobboard");
	 
	public static final String PATH_SEPARATOR = File.separator;
	public static final String LOSTFILE_PREFIX = "ERROR_";
	
	public static String lostFileFolder = BasicConfig.get("lostfile_folder");
	public static String errorLogFile = BasicConfig.get("error_log");
	
	public static String headers = 
			"\"Advertiser Id\"," +
			"\"AD Id\"," +
			"\"Reference No\"," +
			"\"Action\"," +
			"\"Code\"," +
			"\"Message\"," +
			"\"Status\"," +
			"\"Happend At\"" ;
	
	
	public static ReentrantLock errorLock = new ReentrantLock();
	
	
	private static LogWriter errorWriter = LogWriter.create(errorLogFile);
	
	
	
	private static FileLog logger = new FileLog();
	
	
	public static FileLog getLogger(){
		
		return logger;
		
	}
	
	
	
	public void logErrorToFile(LogItem logItem,String detailContent,String type){
		Writer output = null;
		try {
			if(StringUtils.isEmpty(lostFileFolder)){
				lostFileFolder = "logs" + PATH_SEPARATOR;
			}else if(! lostFileFolder.endsWith(PATH_SEPARATOR)){
				lostFileFolder += PATH_SEPARATOR;
			}
			output = new FileWriter(lostFileFolder  
					+ LOSTFILE_PREFIX 
					+ logItem.getAction() 
					+ "_" 
					+ logItem.getAdvertiserId() 
					+ "_" + logItem.getReferenceNo()+"."+type
					);
			IOUtils.write(detailContent, output);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}finally{
			if(output != null){
				try {
					output.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public void logError(LogItem record){
		
		errorLock.lock();
		try{
			record.setStatus("ERROR");
			errorWriter.log(record);
			
		}finally{
			errorLock.unlock();
		}
		
	}
	
	public void finalize(){
		try {
			FileLog.errorWriter.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

	public static LogWriter getErrorWriter() {
		return errorWriter;
	}


	public static void  setErrorWriter(LogWriter errorWriter) {
		FileLog.errorWriter = errorWriter;
	}


	

}
