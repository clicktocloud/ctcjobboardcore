package com.ctc.jobboard.util;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class LogWriter implements Closeable{
	
	private String logfilename;
	private BufferedWriter bw;
	private String headers;
	
	
	public static LogWriter create(String logfilename){
		return create(logfilename,FileLog.headers);
	}
	
	
	public static LogWriter create(String logfilename, String headers){
		
		
		
		LogWriter logWriter  = new  LogWriter(logfilename, headers);
		
		
		
		try {
			File file = new File(logfilename);
			if (!file.exists()) {
				file.createNewFile();
				FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
				logWriter.setBw( new BufferedWriter(fw) );
				logWriter.getBw().write(String.format("%s%n",headers));
				logWriter.getBw().flush();
			}else{
				FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
				logWriter.setBw( new BufferedWriter(fw) );
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return null;
		}
		
		return logWriter;
	}
	
	private LogWriter(String logfilename, String headers){
		this.logfilename = logfilename;
		this.headers = headers;
		
	}
	
	public void log(LogItem record){
		
		if(record == null){
			return;
		}

		File file = new File(logfilename);

		try {
			// if file doesnt exists, then create it
			if (file.exists() && file.length() / 1024  >= BasicConfig.getLogMaxSize()){
				
				this.getBw().close();
				
				//if over size, backup old one and create new one
				file.renameTo(new File(logfilename + "." + new Date().getTime()));
				
				
				//create new one
				File newfile = new File(logfilename);
				if(newfile.createNewFile()){
					FileWriter fw = new FileWriter(newfile.getAbsoluteFile());
					this.setBw(new BufferedWriter(fw));
					this.getBw().write(String.format("%s%n",this.headers));
					this.getBw().flush();
				}
				
			}

			bw.append(String.format("%s%n",record.toString()));
			bw.flush();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	
	@Override
	public void close() throws IOException {
		if(bw != null)
			bw.close();
	}
	
	public void finalize(){
		try {
			close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public BufferedWriter getBw() {
		return bw;
	}
	public void setBw(BufferedWriter bw) {
		this.bw = bw;
	}

}
