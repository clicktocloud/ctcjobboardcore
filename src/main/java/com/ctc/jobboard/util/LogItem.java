package com.ctc.jobboard.util;

import java.util.Date;

public class LogItem {

	private String advertiserId;
	private String adId;
	private String referenceNo;
	private String action;
	private String code;
	private String message;
	private String status;
	
	private Date happenedAt;

	public LogItem(){}
	

	public LogItem(String advertiserId, String adId, String referenceNo,String action,
			String code, String message) {
		super();
		this.advertiserId = advertiserId;
		this.adId = adId;
		this.referenceNo = referenceNo;
		this.action = action;
		this.code = code;
		this.message = message;
		
		this.happenedAt = new Date();
	}
	
//	public LogItem(String advertiserId, String adId, String referenceNo,String action,
//			JobBoardResponse response) {
//		super();
//		this.advertiserId = advertiserId;
//		this.adId = adId;
//		this.referenceNo = referenceNo;
//		this.action = action;
//		
//		response.
//		this.code = code;
//		this.message = message;
//		
//		this.happenedAt = new Date();
//	}



	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAdvertiserId() {
		return advertiserId;
	}



	public void setAdvertiserId(String advertiserId) {
		this.advertiserId = advertiserId;
	}

	public String getAdId() {
		return adId;
	}

	public void setAdId(String adId) {
		this.adId = adId;
	}


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	public Date getHappenedAt() {
		return happenedAt;
	}

	public void setHappenedAt(Date happenedAt) {
		this.happenedAt = happenedAt;
	}


	public String getReferenceNo() {
		return referenceNo;
	}


	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}


	@Override
	public String toString() {
		return 
				"\"" + this.advertiserId + "\"," +
				"\"" + this.adId + "\"," +
				"\"" + this.referenceNo + "\"," +
				"\"" + this.action+ "\"," +
				"\"" + this.code+ "\"," +
				"\"" + this.message+ "\"," +
				"\"" + this.status+ "\"," +
				
				"\"" + String.format("%1$td/%1$tm/%1$tY %1$tT", this.happenedAt)+ "\"";
				
	}

	
	
	
}
