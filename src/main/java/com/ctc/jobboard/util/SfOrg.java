package com.ctc.jobboard.util;

public class SfOrg {
	private String orgId;
	
	

	public SfOrg(String orgId) {
		super();
		this.orgId = orgId;
	}
	
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
}
