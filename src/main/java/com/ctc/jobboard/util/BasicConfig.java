package com.ctc.jobboard.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

public class BasicConfig {
	
	public static final String CONFIG_FILE = "config" + File.separator + "jobboard.conf";
	public static final String CSV_SPLITTER = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
	
	//IMPORTANT: MUST BE false !!!!!
	
	public static String namespace ;
	public static String CORP_ORG_ID ;
	
	public static boolean isTest = true;
	public static String testOrgIds= "00D90000000gtFz";
	
	
	private static Properties properties = new Properties() ;
	private static Long lasttime = 0L;
	
	private static BasicConfig config ;

	
	static{
		BasicConfig.create();
		namespace = get("namespace");
		CORP_ORG_ID = StringUtils.isEmpty( get("corp_org_id") ) ? "00D80000000dN0YEAU" : get("corp_org_id");
		
	}
	
	
	
	public synchronized static BasicConfig create(){
		
		config = new BasicConfig();
		try {
			config.load(CONFIG_FILE);
		} catch (IOException e) {
			//e.printStackTrace();
			System.exit(-1);
		}
		return config;
		
		
	}
	
	public synchronized static BasicConfig getAppConfig(){
	
		if (config == null) {
			create();
		}
		return config;
		
	}
	
	public static Properties getProperties(){
		return BasicConfig.properties;
	}
	
	

	
	
	private BasicConfig(){}
	
	
	public void load() {
		 try {
			load(CONFIG_FILE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
	
	public void  load(String propFileName) throws IOException{
		
		
		
		synchronized (properties) {
			
			try {
				URI uri= getClass().getClassLoader().getResource(propFileName).toURI();
				File file = new File(uri);
				if(file.exists()){
					long lasttime = file.lastModified();
					if(BasicConfig.lasttime == lasttime && properties != null){
						return;
					}
					
					BasicConfig.lasttime = lasttime;
					
				}else{
					return;
				}
			} catch (URISyntaxException e) {
				
			}
			
			InputStream inputStream = null;
			try {
				inputStream = getClass().getClassLoader().getResourceAsStream(
						propFileName);

				load(inputStream);
				
				

			} finally {
				if (inputStream != null) {

					inputStream.close();

				}
			}
		}

	}
	
	public  void  loadFromFileSystem(String realfile) throws IOException{
		
		synchronized (properties) {
			
			
			InputStream inputStream = null;
			try {
				
				inputStream = new FileInputStream(realfile);
				
				load(inputStream);

				

			} finally {
				if (inputStream != null) {

					inputStream.close();

				}
			}
		}

	}
	
	private static void  load(InputStream inputStream) throws IOException{
		if (inputStream != null) {
			
			properties.clear();
			
			//System.out.println("Read properties from file : " + propFileName);
			properties.load(inputStream);

			

		} else {
			
			throw new FileNotFoundException("Failed to inputstream is null !");
		}
	}
	
	public static String get(String propName){
		getAppConfig().load();
		return properties.getProperty(propName);
	}
	
	
	
	
	public static double getLogMaxSize(){
		String value = get("log_max_size");
		if(StringUtils.isEmpty(value)){
			value = "1024";
		}
		Double ret = 1024D;
		
		try {
			ret = Double.valueOf(value.trim());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}
	
	
	
	public static int getHttpConnectionTimeout(){
		String value = get("http_connection_timeout");
		if(StringUtils.isEmpty(value)){
			value = "30";
		}
		int ret = 30;
		
		try {
			ret = Integer.valueOf(value.trim());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public static int getHttpSocketTimeout(){
		String value = get("http_socket_timeout");
		if(StringUtils.isEmpty(value)){
			value = "90";
		}
		int ret = 90;
		
		try {
			ret = Integer.valueOf(value.trim());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}
	
	
	
	
	
}
