package com.ctc.jobboard.core;

import javax.xml.bind.JAXBException;

import com.ctc.jobboard.marshaller.MarshallerHelper;

public class JobContentXmlConverter implements JobContentConverter{
	
	@Override
	public String convertTo(JobBoardRequest request) throws JAXBException {
		
		return  MarshallerHelper.convertObjecttoXML(request);
		
	}

	@Override
	public <T> T convertFrom(String xmlContent, Class<T> clazz) throws JAXBException {
		if(xmlContent != null && !xmlContent.startsWith("<")){
			xmlContent = xmlContent.substring(xmlContent.indexOf("<"));
		}
		return MarshallerHelper.convertXMLtoObject(xmlContent, clazz);
	}

}
