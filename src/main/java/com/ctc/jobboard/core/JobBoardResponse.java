package com.ctc.jobboard.core;

import java.util.HashMap;
import java.util.Map;

public class JobBoardResponse {
	
	private Integer httpStatusCode;
	private String httpMessage ;
	private Map<String, String> httpHeaders = new HashMap<String, String>();

	public Integer getHttpStatusCode() {
		return httpStatusCode;
	}

	public void setHttpStatusCode(Integer httpStatusCode) {
		this.httpStatusCode = httpStatusCode;
	}

	public String getHttpMessage() {
		return httpMessage;
	}

	public void setHttpMessage(String httpMessage) {
		this.httpMessage = httpMessage;
	}
	
	public void addHeader(String name, String value){
		httpHeaders.put(name, value);
	}
	
	public String getHeader(String name){
		return httpHeaders.get(name);
	}
}
