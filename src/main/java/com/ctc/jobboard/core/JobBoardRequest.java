package com.ctc.jobboard.core;

/**
 * The super type of request to remove job boarding services, any request object to access remote job boarding services must be the sub type of this interface
 * 
 * @author andy
 *
 */
public interface JobBoardRequest {

}
