package com.ctc.jobboard.core;

/**
 * This class to hold the information of credentials to be used to access the remote job board services
 * 
 * @author andy
 *
 */
public class JobBoardCredential {

	//org id
	private String orgId;
	//namespace of salesforce instance
	private String namespace;
	//advertiser id defined by remote services provider
	private String advertiserId;
	//client id /key
	private String clientId;
	//client secret
	private String clientSecret;
	//access token
	private String accessToken;
	
	
	
	public JobBoardCredential(String orgId, String namespace,
			String advertiserId, String clientId, String clientSecret) {
		super();
		this.orgId = orgId;
		this.namespace = namespace;
		this.advertiserId = advertiserId;
		this.clientId = clientId;
		this.clientSecret = clientSecret;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public String getAdvertiserId() {
		return advertiserId;
	}
	public void setAdvertiserId(String advertiserId) {
		this.advertiserId = advertiserId;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getClientSecret() {
		return clientSecret;
	}
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	

}
