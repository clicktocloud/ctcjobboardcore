package com.ctc.jobboard.core;

import javax.xml.bind.JAXBException;

/**
 * This component is an interface which declares two methods to convert JobBoardRequest/string to/from string/object. It must be implemented when you implement job board plugin.
 * For jxt plugin, It will process xml string
 * For seek plugin, It will process json string

 * @author andy
 *
 */
public interface JobContentConverter {
	
	/**
	 * convert a JobBoardRequest to string
	 * @param request
	 * @return
	 * @throws JAXBException
	 */
	public String convertTo(JobBoardRequest request) throws JAXBException;
	
	/**
	 * convert a string to generic type
	 * @param jobContent
	 * @param clazz
	 * @return
	 * @throws JAXBException
	 */
	public <T> T convertFrom(String jobContent, Class<T> clazz) throws JAXBException;

}
