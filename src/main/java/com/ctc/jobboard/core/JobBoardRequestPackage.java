package com.ctc.jobboard.core;

import java.util.HashMap;
import java.util.Map;

public class JobBoardRequestPackage {
	
	public static  enum MethodType{GET, POST, HEAD,PUT,PATCH};
	
	private MethodType methodType;
	private String contentType;
	private Map<String, String> headers = new HashMap<String, String>();

	private JobBoardRequest request;
	
	private String body;
	
	private String url;
	
	
	
	public void addHeader(String key, String value){
		headers.put(key, value);
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}



	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public JobBoardRequest getRequest() {
		return request;
	}

	public void setRequest(JobBoardRequest request) {
		this.request = request;
	}

	public MethodType getMethodType() {
		return methodType;
	}

	public void setMethodType(MethodType methodType) {
		this.methodType = methodType;
	}
	
	public boolean isGet(){
		return this.methodType == MethodType.GET;
	}
	
	public boolean isPOST(){
		return this.methodType == MethodType.POST;
	}
	
	
	public boolean isHead(){
		return this.methodType == MethodType.HEAD;
	}
	
	public boolean isPut(){
		return this.methodType == MethodType.PUT;
	}
	
	public boolean isPatch(){
		return this.methodType == MethodType.PATCH;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
	

}
