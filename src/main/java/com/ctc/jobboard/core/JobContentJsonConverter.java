package com.ctc.jobboard.core;

import javax.xml.bind.JAXBException;

import com.ctc.jobboard.marshaller.MarshallerHelper;

public class JobContentJsonConverter implements JobContentConverter{

	@Override
	public String convertTo(JobBoardRequest request) throws JAXBException {
		
		return  MarshallerHelper.convertObjecttoJson(request);
		
	}

	@Override
	public <T> T convertFrom(String jobContent, Class<T> clazz) throws JAXBException {
		
		return MarshallerHelper.convertJsontoObject(jobContent, clazz);
	}

}
