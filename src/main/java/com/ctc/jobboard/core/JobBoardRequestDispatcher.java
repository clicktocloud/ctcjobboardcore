package com.ctc.jobboard.core;


import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ctc.jobboard.adapter.RestHttpClient;
import com.ctc.jobboard.exception.BadRequestException;

/**
 * This component responses to 
 * use specified JobContentConverter to convert JobBoardRequestPackage request to string to be used to build the body of restful request.
 * call corresponding http methods, such as PUT, GET, POST,PATCH and HEAD, according to the properties of JobBoardRequestPackage request.
 * invoke RestHttpClient instance to execute a restful request  and converting http response to a JobBoardResponse object.
 * 
 * @author andy
 *
 */
public class JobBoardRequestDispatcher {
	

	
	private Logger logger = LoggerFactory.getLogger(JobBoardRequestDispatcher.class);
	
	//the instance of restful client
	private RestHttpClient rsClient;

	/**
	 * Constructor with default connection settings
	 */
	public JobBoardRequestDispatcher( ) {
		 
		this.setRsClient(new RestHttpClient());
	}
	
	/**
	 * Constructor with specified settings
	 * @param minPoolSize
	 * @param maxPoolSize
	 * @param connectionTimeout
	 * @param socketTimeout
	 */
	public JobBoardRequestDispatcher(int minPoolSize, int maxPoolSize, int connectionTimeout, int socketTimeout) {
		
		this.setRsClient(new RestHttpClient(minPoolSize, maxPoolSize, connectionTimeout, socketTimeout));
	}
	
	/**
	 * add a success code to rsClient to identify if the request is handled successfully 
	 * @param code
	 */
	public void addSuccessCode(Integer code){
		if(rsClient != null){
			rsClient.addSuccessCode(code);
			
		}
	}
	
	/**
	 * add a set of success codes to identify if the request is handled successfully 
	 * @param codes
	 */
	public void addSuccessCodes(Set<Integer> codes){
		if(rsClient != null){
			rsClient.addSuccessCodes(codes);
			
		}
	}
	
	
	@Deprecated
	public <R> R execute(JobBoardRequest request, Class<R> clazz, JobContentConverter contentConverter) throws BadRequestException {
		
		String bodyString = null;
		String xmlResult = "";
    	try {
    		bodyString = contentConverter.convertTo(request);
    	
    		//System.out.println(bodyString);
    		String serviceEndpoint = JobBoard.requestEndpoints.get(request.getClass());
    		//System.out.println(serviceEndpoint);
    		if(serviceEndpoint != null){
    			long begin = System.currentTimeMillis();
    			xmlResult = rsClient.post( serviceEndpoint, bodyString);
    			logger.debug("Response - timecost: ["+(System.currentTimeMillis() - begin)+" ms]");
    			
    			
    			if(!xmlResult.startsWith("<")){
    				xmlResult = xmlResult.substring(xmlResult.indexOf("<"));
    			}
    			
        		R r = contentConverter.convertFrom(xmlResult, clazz);
        		
        		return r;
    			
    		}else{
    			throw new BadRequestException(-1, "Invalid request type - no service endpoint found !");
    		}
    		
    		
    	}catch(BadRequestException e){
    		
    		logger.error(e.getMessage());
    		
    		throw e;
    	}  catch(JAXBException e){
    		
    		logger.error("Parse request object failed.", e);
    		logger.error(xmlResult);
    		throw new BadRequestException(-2, "Parse request/response object failed !");
    	} catch (Exception e) {
    		
    		logger.error("Unknown error." + e.getMessage(), e);
    		//logger.debug( bodyString );
    		throw new BadRequestException(-3, "Unknown error." + e.getMessage());
    	}
	}
	
	/**
	 * execute a JobBoardRequestPackage and convert result to 
	 * @param requestPackage
	 * @param clazz
	 * @param contentConverter
	 * @return
	 * @throws BadRequestException
	 */
	public <R extends JobBoardResponse> R execute(JobBoardRequestPackage requestPackage, Class<R> clazz, JobContentConverter contentConverter) throws BadRequestException {
		R r = null;
		
		String result = "";
    	try {
    		JobBoardRequest request = requestPackage.getRequest();
    		
    		
    		String serviceEndpoint = requestPackage.getUrl();
    		if((serviceEndpoint == null || serviceEndpoint.equals("")) 
    				&& request != null){
    			serviceEndpoint = JobBoard.requestEndpoints.get(request.getClass());
    		}

    		if(serviceEndpoint != null){

    			Map<String, String> headers = requestPackage.getHeaders();
   			
    			long begin = System.currentTimeMillis();
    			
    			if(requestPackage.isPOST() || requestPackage.isPut() | requestPackage.isPatch()){
    				String bodyString = requestPackage.getBody();
    				if((bodyString == null || bodyString.equals("")) 
    						&& request != null){
    				
    					bodyString = contentConverter.convertTo(request);
        				
    				}
    				if(requestPackage.isPut()){
    					result = rsClient.put(serviceEndpoint, bodyString,  requestPackage.getContentType(), headers);
    					
    				}else if(requestPackage.isPatch()){
    					result = rsClient.patch(serviceEndpoint, bodyString,  requestPackage.getContentType(), headers);
    				}else{
    					result = rsClient.post(serviceEndpoint, bodyString,  requestPackage.getContentType(), headers);
    				}
    				
    				
    			}else if(requestPackage.isGet()){
    				result = rsClient.get(serviceEndpoint, headers);
    			}else if(requestPackage.isHead()){
    				Map<String, String> httpHeaders = rsClient.head(serviceEndpoint, headers);
    				r = createResponseWithHttpHeaders(clazz, httpHeaders);
    				
    			}else{
    				throw new BadRequestException(-1, "Invalid request method type  !");
    			}
    			
    			logger.debug("Response - timecost: ["+(System.currentTimeMillis() - begin)+" ms]");
    			
        		
        		if(! StringUtils.isEmpty( result )){
        			try {
						r = contentConverter.convertFrom(result, clazz);
					} catch (JAXBException e) {
						e.printStackTrace();
						System.out.println(result);
						throw new BadRequestException(-3, "Unexpeced response content : " + e.getMessage());
					}
        		}

        		return r;
    			
    		}else{
    			throw new BadRequestException(-1, "Invalid request type - no service endpoint found !");
    		}
    		
    		
    	}catch(BadRequestException e){
    		
    		logger.error(e.getStatusCode() +" - " + e.getMessage());
    		throw e;
    	}  catch(JAXBException e){
    		logger.error(result,e);
    		throw new BadRequestException(-2, "Failed to parse request !");
    	} catch (Exception e) {
    		
    		logger.error("Unknown error." + e.getMessage(), e);
    		throw new BadRequestException(-4, "Unknown error." + e.getMessage());
    	}
	}
	
	
	private <R extends JobBoardResponse> R createResponseWithHttpHeaders(Class<R> clazz, Map<String, String> httpHeaders){
		
		try {
			R r = clazz.newInstance();
			
			for( Map.Entry<String, String> entry : httpHeaders.entrySet()){
				r.addHeader(entry.getKey(), entry.getValue());
			}
			
			return r;
		} catch (InstantiationException | IllegalAccessException e) {
			
		}
		
		return null;
		
	}

	public RestHttpClient getRsClient() {
		return rsClient;
	}

	public void setRsClient(RestHttpClient rsClient) {
		this.rsClient = rsClient;
	}
}
