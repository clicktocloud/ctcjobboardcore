package com.ctc.jobboard.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.component.AccountFactory;
import com.ctc.jobboard.component.IAccount;
import com.ctc.jobboard.exception.AccountException;
import com.ctc.jobboard.exception.BadRequestException;
import com.ctc.jobboard.exception.JobBoardException;
import com.ctc.jobboard.persistence.JBConnection;
import com.ctc.jobboard.persistence.JBConnectionException;
import com.ctc.jobboard.persistence.JBConnectionManager;
import com.ctc.jobboard.persistence.JBObject;
import com.ctc.jobboard.util.BasicConfig;
import com.ctc.jobboard.util.SfOrg;

/**
 * This abstract class to define a general properties and functions that all job boards could have, such as:
 *  String namespace - the namespace of salesforce instance to be written back;
	IAccount account - the account provided by the provider of job posting service
	JBConnection currentConnection - connection to persistence layer   
	String jobBoardName - the name of job board
	SfOrg currentSforg -  the information of org accessing this web service
	List<JBObject> updateSfList - the objects and fields to be saved to persistency layer
	String referenceNo - the referenc no of job
	String actionType - such as create, update and archive
	JobBoardRequestDispatcher requestDispatcher - the dispatcher to dispatch restful request to remote job board services, such jxt, seek
	JobContentConverter contentConverter - to convert http response result string to pojo object
	JobBoardResponse response - the response object 
	JobBoardResponsePackage responsePackage - to package the response and provide some ways to get information from response object
	Set<Integer> successCodes  - what http status codes means that service response is successful
	
	//only for cron job
	String jobBoardSfApiName;
	//only for cron job
	Map<String, String> adStatusMap = new HashMap<String, String>();
	//only for cron job
	Map<String, String> adPostingStatuses = new HashMap<String, String>();
 * 
 * 
 * protected IAccount getAccount( ) - get an account to access the remote job board services
 * protected <R  extends JobBoardResponse> R execute(JobBoardRequestPackage requestPackage, Class<R> clazz) - execute a request to interact to remote job board service
 * public void responseHandler() - handle the response, such as saving response to db, convert response to response package for further usage
 * protected void addArchiveJBObjects(String id) - add the archived job to cache list
 * protected void addFailureJBObjects(String aid, String failureMessage) - add the failed job to cache list
 * protected void addSuccessJBObjects(String aid, String jobBoardId, String onlineAdUrl,String onlineJobId,String operation) - add the successful job to cache list
 * protected void updateSFJobAds( ) - bulk update the cache list to persistence layer
 * 
 * 
 * 
 * @author andy
 *
 */
public abstract class JobBoard {
	static Logger logger = Logger.getLogger("com.ctc.jobboard");	
	public static final String  SF_POSTING_STATUS_ARCHIVED="Archived Successfully";
	public static final String  SF_POSTING_STATUS_SUCCESS="Posted Successfully";
	public static final String SF_POSTING_STATUS_UPDATED_SUCCESS = "Updated Successfully";
	public static final String  SF_POSTING_STATUS_FAILED="Posting Failed";
	public static final String  SF_POSTING_STATUS_UPDATE_FAILED="Update Failed";
	

	public static final String  SF_POSTING_STATUS_INQUEUE="In Queue";
	public static final String  SF_POSTING_STATUS_ARCHIVE_FAILED="Archiving Failed";
	
	
	public static final String  SF_AD_STATUS_ACTIVE="Active";
	public static final String  SF_AD_STATUS_INACTIVE="Inactive";
	public static final String  SF_AD_STATUS_ARCHIVED="Archived";
	
	public static final String ERROR = "ERROR";
	public static final String UPDATE = "UPDATE";
	public static final String INSERT = "INSERT";
	public static final String ARCHIVE = "ARCHIVE";
	
	public static Map<Class<?>, String> requestEndpoints = new HashMap<Class<?>, String>();
	
//	static{
//		try {
//			String driverName = AppConfig.get("jb_connection_driver");
//			driverName = StringUtils.isEmpty(driverName) ? "com.ctc.jobboard.persistence.drivers.SfJBConnection" : driverName;
//			Class.forName(driverName);
//			
//		} catch (ClassNotFoundException e) {
//			logger.error(e);
//		}
//	}
	
	public static String getAdId(String referenceNo){
		String aid = "";
		if( referenceNo!= null && referenceNo.contains(":")){
			aid = referenceNo.split(":")[1];
			
		}
		
		return aid;
	}
	
	public static String getOrgId(String referenceNo){
		String orgId = "";
		if( referenceNo!= null && referenceNo.contains(":")){
			orgId = referenceNo.split(":")[0];
			
		}
		
		return orgId;
	}
	
	
	private String namespace;

	protected IAccount account;
	protected JBConnection currentConnection;   
	protected String jobBoardName;
	//only for cron job
	protected String jobBoardSfApiName;
	//only for cron job
	protected Map<String, String> adStatusMap = new HashMap<String, String>();
	//only for cron job
	protected Map<String, String> adPostingStatuses = new HashMap<String, String>();
	
	protected SfOrg currentSforg; 
	protected List<JBObject> updateSfList = new ArrayList<JBObject>();
	
	protected String referenceNo;
	
	protected String actionType;
	
	protected JobBoardRequestDispatcher requestDispatcher;
	protected JobContentConverter contentConverter;
	
	private JobBoardResponse response;
	protected JobBoardResponsePackage responsePackage;
	
	protected Set<Integer> successCodes = new HashSet<Integer>();
	
	public JobBoard(){
		namespace = BasicConfig.namespace;
	}
	
	public JobBoard(String jobBoardName, JobContentConverter contentConverter){
		this();
		this.jobBoardName = jobBoardName;
		
		this.contentConverter = contentConverter;
		this.requestDispatcher = new JobBoardRequestDispatcher();
	}
	
	public JobBoard(String jobBoardName, String jobBoardSfApiName, JobContentConverter contentConverter){
		this();
		this.jobBoardName = jobBoardName;
		this.jobBoardSfApiName = jobBoardSfApiName;
		this.contentConverter = contentConverter;
		this.requestDispatcher = new JobBoardRequestDispatcher();
	}
	

	
	

	public abstract void makeJobFeeds();
	
	
	
	public abstract void post2JobBoard();
	
	
	@Deprecated
	protected <R> R execute(JobBoardRequest request, Class<R> clazz) throws BadRequestException{
		if(getRequestDispatcher() == null){
			setRequestDispatcher(  new JobBoardRequestDispatcher() );
		}
		
		getRequestDispatcher().addSuccessCodes(this.successCodes);
		
		return getRequestDispatcher().execute(request, clazz, contentConverter);
		
	}
	
	/**
	 * execute a request to interact to remote job board service
	 * @param requestPackage
	 * @param clazz
	 * @return
	 */
	protected <R  extends JobBoardResponse> R execute(JobBoardRequestPackage requestPackage, Class<R> clazz) {
		if(getRequestDispatcher() == null){
			setRequestDispatcher(  new JobBoardRequestDispatcher() );
		}
		
	
		try {
			
			getRequestDispatcher().addSuccessCodes(this.successCodes);
			R r =  getRequestDispatcher().execute(requestPackage, clazz, contentConverter);
			return r;
		} catch (BadRequestException e) {
			try {
				R r = clazz.newInstance();
				r.setHttpStatusCode(e.getStatusCode());
				r.setHttpMessage(e.getMessage());
				return r;
			} catch (InstantiationException | IllegalAccessException e1) {
				logger.error(e1);
			}
		}
		
		return null;
	}
	
	/**
	 * bulk update the cache list to persistence layer
	 * @throws JBConnectionException
	 */
	protected void updateSFJobAds( ) throws JBConnectionException{
		if(getCurrentConnection() == null){
			currentConnection = JBConnectionManager.connect( currentSforg.getOrgId() );
			if(currentConnection == null){
				logger.error("Failed to get connection to database, abort to write back to database !");

				return;
			}
			
		}
		
		try {
			getCurrentConnection().update(updateSfList);
		} catch (JBConnectionException e) {
			for(JBConnectionException.Error error : e.getErrors()){
				logger.error(error.getMessage()+" : "+Arrays.toString(error.getFields()));
			}
			
			throw e;
		}finally{
			updateSfList.clear();
		}

	}
	
	/**
	 * add the archived job to cache list
	 * @param aid
	 */
	protected void addArchiveJBObjects(String aid){
		JBObject obj = new JBObject();
		obj.setObjectName(this.namespace +"Advertisement__c");
		obj.setField("Id", aid);
		obj.setField(this.namespace +"Job_Posting_Status__c", SF_POSTING_STATUS_ARCHIVED);
		
		//sobj.setField(this.namespace+"Status__c", SF_AD_STATUS_ARCHIVED);
		obj.setField(this.namespace +"Ad_Posting_Status_Description__c", "Archived Successfully");
		
		updateSfList.add(obj);
	}
	
	/**
	 * add the failed job to cache list
	 * @param aid
	 * @param failureMessage
	 */
	protected void addFailureJBObjects(String aid, String failureMessage){
		JBObject obj = new JBObject();
		obj.setObjectName(this.namespace +"Advertisement__c");
		obj.setField("Id", aid);
		
		if(SF_POSTING_STATUS_INQUEUE.equals(adPostingStatuses.get(aid))){
			//sobj.setField(this.namespace+"Status__c", SF_AD_STATUS_INACTIVE);
			obj.setField(this.namespace +"Job_Posting_Status__c", SF_POSTING_STATUS_FAILED);
		}else{
			obj.setField(this.namespace +"Job_Posting_Status__c", SF_POSTING_STATUS_UPDATE_FAILED);
		}

		obj.setField(this.namespace +"Ad_Posting_Status_Description__c", failureMessage);
		
		updateSfList.add(obj);
	}
	
	protected void addArchiveFailureJBObjects(String aid, String failureMessage){
		JBObject obj = new JBObject();
		obj.setObjectName(getNamespace() +"Advertisement__c");
		obj.setField("Id", aid);
		
		obj.setField(getNamespace()+"Status__c", JobBoard.SF_AD_STATUS_ACTIVE);
		
		obj.setField(getNamespace() +"Job_Posting_Status__c", JobBoard.SF_POSTING_STATUS_ARCHIVE_FAILED);
		
		obj.setField(getNamespace() +"Ad_Posting_Status_Description__c", failureMessage);
		
		updateSfList.add(obj);
	}
	
	/**
	 * add the successful job to cache list
	 * @param aid
	 * @param jobBoardId
	 * @param onlineAdUrl
	 * @param onlineJobId
	 * @param operation
	 */
	protected void addSuccessJBObjects(String aid, String jobBoardId, String onlineAdUrl,String onlineOtherUrl,String onlineJobId,String operation){
		JBObject obj = new JBObject();
		obj.setObjectName(this.namespace + "Advertisement__c");
		obj.setField("Id", aid);
		obj.setField(this.namespace + "Job_Board_ID__c", jobBoardId);
		if(UPDATE.equals(operation)){
			obj.setField(this.namespace + "Job_Posting_Status__c", SF_POSTING_STATUS_UPDATED_SUCCESS);
			obj.setField(this.namespace +"Ad_Posting_Status_Description__c", SF_POSTING_STATUS_UPDATED_SUCCESS);
		}else{
			obj.setField(this.namespace + "Job_Posting_Status__c", SF_POSTING_STATUS_SUCCESS);
			obj.setField(this.namespace +"Ad_Posting_Status_Description__c",  SF_POSTING_STATUS_SUCCESS);
			if(StringUtils.isBlank(getAdStatusMap().get(aid))){
				//Added the Calender as the date when inserted in Salesforce if the time is less than 10 AM then it converts the date to previous date.
				//So set the time to 11 AM so to avoid the previous date issue.
				Calendar cal = Calendar.getInstance();  
		        cal.setTime(new Date());  
		        cal.set(Calendar.HOUR_OF_DAY, 11);  
		        obj.setField(this.namespace + "Placement_Date__c", cal.getTime());
			}
		}

		
		obj.setField(this.namespace + "Online_Ad__c", onlineAdUrl);
		obj.setField(this.namespace + "Online_Ad_URL__c", onlineOtherUrl);
		obj.setField(this.namespace + "Online_Job_Id__c", onlineJobId);
		
		
		
		updateSfList.add(obj);
	}

	public JBConnection getCurrentConnection() {
		return currentConnection;
	}

	public SfOrg getCurrentSforg() {
		return currentSforg;
	}

	public void setCurrentSforg(SfOrg currentSforg) {
		this.currentSforg = currentSforg;
	}

	public String getNamespace() {
		return namespace == null ? "" : namespace;
	}

	public void setNamespace(String namespace) {
		
		this.namespace = namespace == null ? "" : namespace;
	}


	public Map<String, String> getAdPostingStatuses() {
		return adPostingStatuses;
	}

	public void setAdPostingStatuses(Map<String, String> adPostingStatuses) {
		this.adPostingStatuses = adPostingStatuses;
	}
	
	public JobBoardRequestDispatcher getRequestDispatcher() {
		return requestDispatcher;
	}


	public void setRequestDispatcher(JobBoardRequestDispatcher jxtServiceDispatcher) {
		this.requestDispatcher = jxtServiceDispatcher;
	}

	public JobContentConverter getContentConverter() {
		return contentConverter;
	}

	public void setContentConverter(JobContentConverter contentConverter) {
		this.contentConverter = contentConverter;
	}
	
	public void setAccount(IAccount account) {
		
		this.account = account;
		
		AccountFactory.addAccount( getJobBoardName(), account);	
		
	}
	
	/**
	 * get an account to access the remote job board services
	 * @return
	 * @throws AccountException
	 */
	protected IAccount getAccount( ) throws AccountException{
		String accountId = this.account == null ? "":this.account.getUsername();
		IAccount account =  AccountFactory.getAccount(getCurrentConnection(),  getJobBoardName(),  accountId, getNamespace());
		if(account != null){
			this.account = account;
		}
		
		return account;
		
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public JobBoardResponsePackage getResponsePackage() {
		return responsePackage;
	}

	public void setResponsePackage(JobBoardResponsePackage responsePackage) {
		this.responsePackage = responsePackage;
	}

	public JobBoardResponse getResponse() {
		return response;
	}

	public void setResponse(JobBoardResponse response) {
		this.response = response;
		
	}
	

	/**
	 * handle the response, such as saving response to db, convert response to response package for further usage
	 */
	public void responseHandler(){
		
		JobBoardResponsePackage responsePackage = getResponsePackage();
		
		if(responsePackage.getSummary() != null){
			logger.debug("Sent : " +responsePackage.getSummary().getSent());
			logger.debug("inserted : " +responsePackage.getSummary().getInserted());
			logger.debug("updated : " +responsePackage.getSummary().getUpdated());
			logger.debug("archived : " +responsePackage.getSummary().getArchived());
			logger.debug("Failed : " +responsePackage.getSummary().getFailed());
		}
		
		
		if(responsePackage.hasErrors()){
			logger.info("Failed to post job ads to "+jobBoardName );
			List<String> messages = responsePackage.getErrorMessages();
			if(messages != null){
				for(String m : messages){
					logger.warn(m);
				}
			}
			for(JobBoardResponsePackage.Error error : responsePackage.getJobErrors()){
				logger.warn(error.getCode() + " - " + error.getMessage());
			}
		}else{
			
			logger.info("Success to post job ads to "+jobBoardName );
		}
		
		
		try {
			
			insertUpdateHandler( );
			archivedHanler( );
			failureHandler( );
			
			updateSFJobAds( );
			
		}  catch (JBConnectionException e1) {
			
			throw new JobBoardException("JBConnectionException: " +e1);
		}catch(Exception e){
			
			
			throw new JobBoardException("Exception: " + e);
		}
		
	}

	
	protected void insertUpdateHandler( ) {
		
		
		if(getResponsePackage().hasErrors()){
			return;
		}
		
		for(JobBoardResponsePackage.Advertisement a : getResponsePackage().getInsertUpdated()){
			addSuccessJBObjects(a.getId(),getAccount().getAdvertiserId(), a.getOnlineUrl(), a.getOnlineOtherUrl(),a.getOnlineId() ,a.getAction());
		}
		
		
	}
	
	protected void archivedHanler( )  {
		
		if(getResponsePackage().hasErrors())
			return;
		
		for(JobBoardResponsePackage.Advertisement a : getResponsePackage().getArchived()){
			addArchiveJBObjects(a.getId());
		}
		
	}
	
	protected void failureHandler( ){
		
		
		JobBoardResponsePackage responsePackage = getResponsePackage();
		List<JobBoardResponsePackage.Error> errors = responsePackage.getJobErrors();
		if(errors != null && errors.size() > 0){
			for(JobBoardResponsePackage.Error e : errors){
				//String aid = getAdId(e.getCode());
				String aid = getAdId(this.referenceNo);
				
				if(!StringUtils.isEmpty( aid )){
					if(JobBoard.ARCHIVE.equals(this.actionType)){
						addArchiveFailureJBObjects(aid, e.getMessage());
					}else{
						addFailureJBObjects(aid, e.getMessage());
					}
					
				}
				
			}
		}else{
			String aid = getAdId(this.referenceNo);
			
			String errorMessage = responsePackage.getErrorMessage();
			if(!StringUtils.isEmpty(aid) && ! StringUtils.isEmpty(errorMessage)){
				if(JobBoard.ARCHIVE.equals(this.actionType)){
					addArchiveFailureJBObjects(aid,errorMessage);
				}else{
					addFailureJBObjects(aid,errorMessage);
				}	
			}
		}
	}
	
	public String getJobBoardName() {
		return jobBoardName;
	}

	public void setJobBoardName(String jobBoardName) {
		this.jobBoardName = jobBoardName;
	}

	public String getJobBoardSfApiName() {
		return jobBoardSfApiName;
	}

	public void setJobBoardSfApiName(String jobBoardSfApiName) {
		this.jobBoardSfApiName = jobBoardSfApiName;
	}

	public Map<String, String> getAdStatusMap() {
		return adStatusMap;
	}

	public void setAdStatusMap(Map<String, String> adStatusMap) {
		this.adStatusMap = adStatusMap;
	}

}
