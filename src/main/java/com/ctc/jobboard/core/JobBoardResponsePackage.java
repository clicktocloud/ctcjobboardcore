package com.ctc.jobboard.core;

import java.util.List;

public interface JobBoardResponsePackage {
	
	public JobBoardResponse getResponse();
	
	public Summary getSummary();
	
	public List<Advertisement> getArchived();
	
	public List<Advertisement> getInsertUpdated();
	public List<Advertisement> getCreated();
	public List<Advertisement> getUpdated();
	
	public boolean hasErrors();
	public String getErrorMessage();
	public List<String> getErrorMessages();
	public List<Error> getErrors();
	public List<Error> getJobErrors();
	
	public static class Summary{
		private int sent;
		private int inserted;
		private int updated;
		private int archived;
		private int failed;
		public int getSent() {
			return sent;
		}
		public void setSent(Integer sent) {
			
			this.sent = sent == null ? 0 : sent;
		}
		public int getInserted() {
			return inserted;
		}
		public void setInserted(Integer inserted) {
			this.inserted = inserted == null ? 0 : inserted;
		}
		public int getUpdated() {
			return updated;
		}
		public void setUpdated(Integer updated) {
			this.updated = updated == null ? 0 : updated;
		}
		
		public int getFailed() {
			return failed;
		}
		public void setFailed(Integer failed) {
			this.failed = failed == null ? 0 :failed;
		}
		public int getArchived() {
			return archived;
		}
		public void setArchived(Integer archived) {
			this.archived = archived == null ? 0 : archived;
		}
	}
	
	public static class Advertisement{
		private String id;
		private String onlineId;
		private String onlineUrl;
		private String onlineOtherUrl;
		
		private String action;
		
		private String status;
		
		
		public Advertisement(String id) {
			super();
			this.id = id;
		}
		public Advertisement(String id, String onlineId, String onlineUrl) {
			super();
			this.id = id;
			this.onlineId = onlineId;
			this.onlineUrl = onlineUrl;
		}
		
		public Advertisement(String id, String onlineId, String onlineUrl,
				String action) {
			super();
			this.id = id;
			this.onlineId = onlineId;
			this.onlineUrl = onlineUrl;
			this.action = action;
		}
		
		public Advertisement(String id, String onlineId, String onlineUrl,
				String action,String status) {
			super();
			this.id = id;
			this.onlineId = onlineId;
			this.onlineUrl = onlineUrl;
			this.action = action;
			this.status = status;
		}
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getOnlineId() {
			return onlineId;
		}
		public void setOnlineId(String onlineId) {
			this.onlineId = onlineId;
		}
		public String getOnlineUrl() {
			return onlineUrl;
		}
		public void setOnlineUrl(String onlineUrl) {
			this.onlineUrl = onlineUrl;
		}
		public String getAction() {
			return action;
		}
		public void setAction(String action) {
			this.action = action;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		
		public String getOnlineOtherUrl() {
			return onlineOtherUrl;
		}
		public void setOnlineOtherUrl(String onlineOtherUrl) {
			this.onlineOtherUrl = onlineOtherUrl;
		}
		
	}
	
	public static class Error{
		private String code;
		private String message;
		
		
		
		public Error(String code, String message) {
			super();
			this.code = code;
			this.message = message;
		}
		
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
	}

}
