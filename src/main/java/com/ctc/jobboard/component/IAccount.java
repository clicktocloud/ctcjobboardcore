package com.ctc.jobboard.component;

import java.util.Date;

/**
 * The abstraction of general account accessing to remote job board services
 * 
 * @author andy
 *
 */
public interface IAccount {
	
	
	public void setAdvertiserId(String advertiserId);
	public void setPassword(String password);
	public void setUsername(String username);
	public void setAccessToken(String accessToken);
	public void setAccessTokenExpireIn(int seconds);
	public void setAccessTokenFrom(Date from);
	
	public String getUsername() ;

	public String getPassword() ;

	public String getAdvertiserId();

	public String getAccessToken() ;

	public int getAccessTokenExpireIn();

	public Date getAccessTokenFrom() ;

}
