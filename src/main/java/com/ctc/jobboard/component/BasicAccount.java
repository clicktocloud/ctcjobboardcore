package com.ctc.jobboard.component;

import java.util.Date;

public  class BasicAccount implements IAccount {
	
	private String username;
	private String password;
	private String advertiserId;
	private String accessToken;
	private int accessTokenExpireIn;
	private Date accessTokenFrom;
	
	public BasicAccount(){
		
	}
	
	public BasicAccount(String username, String password, String advertiserId){
		this.username = username;
		this.password = password;
		this.advertiserId = advertiserId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAdvertiserId() {
		return advertiserId;
	}

	public void setAdvertiserId(String advertiserId) {
		this.advertiserId = advertiserId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public int getAccessTokenExpireIn() {
		return accessTokenExpireIn;
	}

	public void setAccessTokenExpireIn(int accessTokenExpireIn) {
		this.accessTokenExpireIn = accessTokenExpireIn;
	}

	public Date getAccessTokenFrom() {
		return accessTokenFrom;
	}

	public void setAccessTokenFrom(Date accessTokenFrom) {
		this.accessTokenFrom = accessTokenFrom;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicAccount other = (BasicAccount) obj;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	

	

}
