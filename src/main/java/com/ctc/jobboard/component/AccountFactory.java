package com.ctc.jobboard.component;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.exception.AccountException;
import com.ctc.jobboard.persistence.JBConnection;
import com.ctc.jobboard.persistence.JBConnectionException;
import com.ctc.jobboard.persistence.JBConnectionManager;

/**
 * This component is responsible for managing the account to access remote job board services:
 * Keep all accounts in memory for the later usage.
 * If need, it will retrieve account information from database/salesforce.
 * 
 * @author andy
 *
 */
public class AccountFactory {
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	private static Map<String, IAccount> accounts = new HashMap<String, IAccount>();
	
	/**
	 * add an account to account map;
	 * @param jobBoardName
	 * @param a
	 */
	public static  void addAccount(String jobBoardName,  IAccount a){
		
		if( jobBoardName == null){
			return;
		}

		synchronized (accounts) {
			String key = generateAccountKey(jobBoardName , a.getUsername());
			
			IAccount oldAccount = accounts.get(key);
			if(oldAccount == null || !oldAccount.equals(a)){
				accounts.put(key, a);
			}
			
		}
		
	}
	
	/**
	 * get an account from account map or database by org id 
	 * @param orgId
	 * @param jobBoardName
	 * @param accountId
	 * @param namespace
	 * @return
	 * @throws AccountException
	 */
	public static IAccount getAccount(String orgId, String jobBoardName, String accountId, String namespace) throws AccountException{
		
		synchronized (accounts) {
			
			IAccount a = null;
			if(!StringUtils.isEmpty( jobBoardName ) ){
				a = accounts.get( generateAccountKey( jobBoardName , accountId) );
			}
			
			if (a == null && !StringUtils.isEmpty( jobBoardName )) {

				try{
					JBConnection conn = JBConnectionManager.connect(orgId);

					a = (IAccount) conn.getAccount(jobBoardName, namespace);
					if(a != null)
						accounts.put(generateAccountKey(jobBoardName ,accountId) , a);
					
				}catch(JBConnectionException e){
					logger.error("Failed to connect to salesforce instance of org["+orgId+"]");
					throw new AccountException("Failed to connect to salesforce instance of org["+orgId+"]");
				}
			}
			return a;
		}

	}
	
	/**
	 * get an account from account map or database by JBConnection 
	 * @param conn
	 * @param jobBoardName
	 * @param accountId
	 * @param namespace
	 * @return
	 * @throws AccountException
	 */
	
	public static IAccount getAccount(JBConnection conn,  String jobBoardName, String accountId, String namespace) throws AccountException{
		
		synchronized (accounts) {
			IAccount a = null;
			if(!StringUtils.isEmpty( jobBoardName ) ){
				a = accounts.get( generateAccountKey( jobBoardName , accountId) );
			}
			
			if (a == null && !StringUtils.isEmpty( jobBoardName )) {
				
				try {
					a = (IAccount) conn.getAccount( jobBoardName, namespace);
				} catch (JBConnectionException e) {
					throw new AccountException(e);
				}
				
				if( a != null)
					accounts.put(generateAccountKey(jobBoardName ,accountId ) , a);

			}
			return a;
		}
	}
	
	/**
	 * generate a key to put account into account map
	 * @param jobBoardName
	 * @param accountId
	 * @return
	 */
	private static String generateAccountKey(String jobBoardName,String accountId){
		if(!StringUtils.isEmpty(accountId)){
			return jobBoardName +":" + accountId;
		}else{
			return jobBoardName;
		}
	}

}
