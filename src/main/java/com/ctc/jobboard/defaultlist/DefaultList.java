package com.ctc.jobboard.defaultlist;


public class DefaultList {
	private JobTemplateList jobTemplateList;
	private WorkTypeList workTypeList;
	private AdvertiserJobTemplateLogoList advertiserJobTemplateLogoList;
	private ClassificationList classificationList;
	private CountryList countryList;
	private SalaryTypeList salaryTypeList;
	private SalaryRange  salaryRange;
	
	
	public JobTemplateList getJobTemplateList() {
		return jobTemplateList;
	}
	public void setJobTemplateList(JobTemplateList jobTemplateList) {
		this.jobTemplateList = jobTemplateList;
	}
	public WorkTypeList getWorkTypeList() {
		return workTypeList;
	}
	public void setWorkTypeList(WorkTypeList workTypeList) {
		this.workTypeList = workTypeList;
	}
	public AdvertiserJobTemplateLogoList getAdvertiserJobTemplateLogoList() {
		return advertiserJobTemplateLogoList;
	}
	public void setAdvertiserJobTemplateLogoList(
			AdvertiserJobTemplateLogoList advertiserJobTemplateLogoList) {
		this.advertiserJobTemplateLogoList = advertiserJobTemplateLogoList;
	}
	public ClassificationList getClassificationList() {
		return classificationList;
	}
	public void setClassificationList(ClassificationList classificationList) {
		this.classificationList = classificationList;
	}
	public CountryList getCountryList() {
		return countryList;
	}
	public void setCountryList(CountryList countryList) {
		this.countryList = countryList;
	}
	public SalaryTypeList getSalaryTypeList() {
		return salaryTypeList;
	}
	public void setSalaryTypeList(SalaryTypeList salaryTypeList) {
		this.salaryTypeList = salaryTypeList;
	}
	public SalaryRange getSalaryRange() {
		return salaryRange;
	}
	public void setSalaryRange(SalaryRange salaryRange) {
		this.salaryRange = salaryRange;
	}

}
