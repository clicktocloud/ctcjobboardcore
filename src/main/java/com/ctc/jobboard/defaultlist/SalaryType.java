
package com.ctc.jobboard.defaultlist;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.ctc.jobboard.defaultlist.SalaryRange.Range;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalaryType", propOrder = {
    "salaryTypeID",
    "salaryTypeName",
    "annualRanges",
    "hourlyRanges"
})
public class SalaryType {

    

	@XmlElement(name = "SalaryTypeID", nillable = true)
    protected String salaryTypeID;
    @XmlElement(name = "SalaryTypeName", nillable = true)
    protected String salaryTypeName;
    
    @XmlElement(name = "AnnualRanges", nillable = true)
	private List<Range> annualRanges;
	@XmlElement(name = "HourlyRanges", nillable = true)
	private List<Range> hourlyRanges;

    public SalaryType() {
		super();
		
	}
    
    

	public SalaryType(String salaryTypeID, String salaryTypeName) {
		super();
		this.salaryTypeID = salaryTypeID;
		this.salaryTypeName = salaryTypeName;
	}



	public String getSalaryTypeID() {
		return salaryTypeID;
	}

	public void setSalaryTypeID(String salaryTypeID) {
		this.salaryTypeID = salaryTypeID;
	}

	public String getSalaryTypeName() {
		return salaryTypeName;
	}

	public void setSalaryTypeName(String salaryTypeName) {
		this.salaryTypeName = salaryTypeName;
	}

	public List<Range> getAnnualRanges() {
		return annualRanges;
	}



	public void setAnnualRanges(List<Range> annualRanges) {
		this.annualRanges = annualRanges;
	}



	public List<Range> getHourlyRanges() {
		return hourlyRanges;
	}



	public void setHourlyRanges(List<Range> hourlyRanges) {
		this.hourlyRanges = hourlyRanges;
	}
}
