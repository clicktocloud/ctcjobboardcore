
package com.ctc.jobboard.defaultlist;


import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalaryTypeList", propOrder = {
    "salaryType"
})
@XmlRootElement(name="SalaryTypeList")
public class SalaryTypeList {

    @XmlElement(name = "SalaryType", nillable = true)
    protected List<SalaryType> salaryType;

   
    public List<SalaryType> getSalaryType() {
        if (salaryType == null) {
        	salaryType = new LinkedList<SalaryType>();
        }
        return this.salaryType;
    }

}
