package com.ctc.jobboard.defaultlist;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.StringUtils;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SalaryRange", propOrder = {
    "name",
    "bands"
})
public class SalaryRange {
	
	@XmlElement(name = "Name", nillable = true)
	private String name;
	@XmlElement(name = "Bands", nillable = true)
	private List<Band> bands;
	
	public SalaryRange() {
		
	}

	public SalaryRange(String name) {
		super();
		this.name = name;
	}


	public void addBand(Band band) {
		if(this.bands == null){
			this.bands = new LinkedList<Band>();
		}
		
		this.bands.add(band);
	}
	
	
	public List<Band> getBands() {
		return bands;
	}

	public void setBands(List<Band> bands) {
		this.bands = bands;
	}

	
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "Range", propOrder = {
	    "from",
	    "to"
	})
	public static class Range{
		
		@XmlElement(name = "From")
		private BigDecimal from;
		@XmlElement(name = "To", nillable = true)
		private BigDecimal to;
		
		public Range(String from, String to) {
			super();
			if(!StringUtils.isEmpty(from) && !StringUtils.isEmpty(from.trim())){
				
				this.from = new BigDecimal(from.replace(",",""));
			}
			
			if(!StringUtils.isEmpty(to)  && !StringUtils.isEmpty(to.trim())){
				
				this.to = new BigDecimal(to.replace(",",""));
			}
				
		}
		
		public Range(BigDecimal from, BigDecimal to) {
			super();
			this.from = from;
			this.to = to;
		}
		public BigDecimal getFrom() {
			return from;
		}
		public void setFrom(BigDecimal from) {
			this.from = from;
		}
		public BigDecimal getTo() {
			return to;
		}
		public void setTo(BigDecimal to) {
			this.to = to;
		}
		
		
	}
	
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "Band", propOrder = {
	    "name",
	    "annualRanges",
	    "hourlyRanges"
	})
	public static class Band{
		@XmlElement(name = "Name", nillable = true)
		private String name;
		@XmlElement(name = "AnnualRanges", nillable = true)
		private List<Range> annualRanges;
		@XmlElement(name = "HourlyRanges", nillable = true)
		private List<Range> hourlyRanges;
		
		
		
		public Band(String name) {
			super();
			this.name = name;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		public void addAnnualRange(Range range) {
			if(this.annualRanges  == null){
				this.annualRanges = new LinkedList<Range>();
			}
			
			this.annualRanges.add(range);
			
		}
		
		public List<Range> getAnnualRanges() {
			return annualRanges;
		}
		public void setAnnualRanges(List<Range> annualRanges) {
			this.annualRanges = annualRanges;
		}
		
		public void addHourlyRange(Range range) {
			if(this.hourlyRanges  == null){
				this.hourlyRanges = new LinkedList<Range>();
			}
			
			this.hourlyRanges.add(range);
			
		}
		
		public List<Range> getHourlyRanges() {
			return hourlyRanges;
		}
		public void setHourlyRanges(List<Range> hourlyRanges) {
			this.hourlyRanges = hourlyRanges;
		}
		
		
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

}
